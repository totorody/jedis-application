import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

import java.util.Scanner;

public class Main {

    public static Jedis tryToConnectRedis(JedisPool pool) {
        try {
            return pool.getResource();
        } catch (Exception exception) {
            return null;
        }
    }

    public static void exitApplication() {
        System.out.println("Exit Redis Application... See ya!");
        System.exit(0);
    }

    public static void askContinue(Scanner s) {
        System.out.print("Continue? (y:1, n:0) ");
        int isContinue = s.nextInt();
        if (isContinue == 0) {
            exitApplication();
        }
    }

    public static void setBillingInfo(Jedis jedis) {
        Scanner s = new Scanner(System.in);

        System.out.print("Please Enter ID: ");
        String id = s.nextLine();

        if (jedis.get(id) != null) {
            _updateBillingInfo(jedis, id);
        } else {
            _writeBillingInfo(jedis, id);
        }

        askContinue(s);
    }

    public static void _updateBillingInfo(Jedis jedis, String id) {

        Scanner s = new Scanner(System.in);
        String prevBillingInfo = jedis.get(id);

        if (prevBillingInfo == null) {
            System.out.println("Can not find ID(" + id + ")");
            return;
        }

        System.out.print("Please Enter Price: ");
        int additionalPrice = s.nextInt();
        String newBillingInfo = Integer.toString(Integer.parseInt(prevBillingInfo) + additionalPrice);

        jedis.set(id, newBillingInfo);
    }

    public static void _writeBillingInfo(Jedis jedis, String id) {
        Scanner s = new Scanner(System.in);

        System.out.print("Please Enter Price: ");
        int price = s.nextInt();
        jedis.set(id, Integer.toString(price));
        System.out.println("Save ID & Price Success");
    }

    public static void lookupBillingInfo(Jedis jedis) {
        Scanner s = new Scanner(System.in);

        System.out.print("Please Enter ID: ");
        String id = s.nextLine();
        String billingInfo = jedis.get(id);

        if (billingInfo != null) {
            System.out.print(id + "'s billing information: ");
            System.out.println(billingInfo);
        } else {
            System.out.println("Can not find ID(" + id + ")");
        }

        askContinue(s);
    }

    public static void deleteBillingInfo(Jedis jedis) {
        Scanner s = new Scanner(System.in);

        System.out.print("Please Enter ID: ");
        String id = s.nextLine();
        String billingInfo = jedis.get(id);

        if (billingInfo != null) {
            jedis.del(id);
            System.out.println("Remove Success");
        } else {
            System.out.println("ID(" + id + ") NOT EXIST");
        }

        askContinue(s);
    }

    public static void main(String[] args) {
        //access step
        System.out.println("Redis Application");

        System.out.println("=========================");
        System.out.println("Redis Connection Step");

        Scanner sc = new Scanner(System.in);

        // Connection Step
        JedisPool pool = new JedisPool(new JedisPoolConfig(), "localhost");

        System.out.println("Try to connect with Redis...");
        Jedis jedis = tryToConnectRedis(pool);
        if (jedis == null) {
            System.out.println("[CONNECTION_ERROR] Failed to connect with Redis.");
            System.exit(1);
        }

        while (true) {
            System.out.println("=======Instruction=======");
            System.out.println("1. ADD billing information");
            System.out.println("2. Lookup billing information");
            System.out.println("3. Remove billing information");
            System.out.println("4. Exit Program");
            System.out.println("=========================");
            System.out.print("Please enter the instruction to run: ");

            int inst = sc.nextInt();
            if (inst == 1) {
                setBillingInfo(jedis);
            } else if (inst == 2) {
                lookupBillingInfo(jedis);
            } else if (inst == 3) {
                deleteBillingInfo(jedis);
            } else if (inst == 4) {
                break;
            } else {
                System.out.println("Wrong instruction... Please retry to select among 1~4 instructions.");
            }
        }

        jedis.close();
        pool.close();
        exitApplication();
    }
}
